# Django Two Shot

# Feature 3 -Models
## Create three models 

    All go in the receipts django app. Account is to hold info about an account to hold money like a bank account or credit card account.
    [x] Expense Category
    [x] Recipts
    [x] Account

## Expense Category Model

    [x] Name - max characters 50
    [x] owner - foreign key to the user model
        [x]related name "categories"
        [x] On_delete Cascade
    [x] __str__ method for name

## Account Model
- way that we paid for it, such as with a specific credit card or a bank account.

        [x] Name - max characters 100
        [x] number - max length of 20 characters
        [x] owner - foreign key to the user model
            [x] related name "accounts
            [x] on delete cascade
        [x] __str__ method for name

## Receipt Model
- primary thing that this application keeps track of for accounting purposes.

- This model introduces the DecimalField which is the kind of field that you always want to store money values in.

        [x] Vendor - max character length of 200
        [x] total - Decimal Field 
            [x] three decimal places
            [x] 10 digit max
        [x] Tax - Decimal Field
            [x] three decimal places
            [x] 10 digit max
        [x] Date - Takes date when transaction took place
        [x] purchaser - Foreign Key to User Model
            [x] related name "recipts"
            [x] on delete cascade
        [x]category - foreign key to ExpenseCategory
            [x] related name "receipts"
            [x] on delete cascade
        [x]account - foreign key Account
            [x] related name "receipts
            [x] on delete cascade
            [x] allowed to be null
    
# Feature 4-Post to admin [x]

# Feature 5 
- This feature is about creating a list view for the Receipt model, registering the view for a path, registering the receipts paths with the expenses project, and creating a template for the view.
    
        [x] Create a view that will get all instance of the Receipt Model and puts them in the context for the template.
        [x] Register that view in the receipts app for path "" and the name "home" in new file named receipts/urls.py
        [x] Include the URL patterns from the receipt app in the expenses receipt with the prefix "recepts/"
        [x] create a remplate for the list view that compiles the following specs
            [x] fun five
            [x] main tag
                [x] h1 MY Receipts
                [x] Table that has 6 columns
                    [x] Headers
                        [x] Vendor - and rows with the vendor name of the receipt
                        [x] Total - total value from the receipt (dont have info yet)
                        [x] Tax - tax value from the receipt
                        [x] Due Date - formatted "month/day/year"- m/d/YY
                        [x] Category - category name
                        [x] Account - name of account for the receipt

# Feature 6 - Home
- This feature is about redirecting http://localhost:8000/  to the project list page created in Feature 5.

    [x] In the expenses urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".

# Feature 7 - Login View
- This feature is about setting up a login page so that the person using the application can be identified.

        [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
        [x] Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/"
        [x] Create a templates directory under account
        [x] Create a registration directory under templates
        [x] Create an HTML template named login.html in the registration directory
        [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
        [x] In the expenses settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"

## Template Specs

    [x] Fun Five
    [x] main tag
        [x] Div Tage
            [x] H1
            [x] Form
                [x] Post Method
                [x] input tag type text and name username
                [x] input tag type password name password
                [x] button with content login

# Feature 8 Filtering Receipts
- This feature requires someone to login before accessing the list view for the receipts. It also constrains the result set of receipts to those that the person is a purchaser of.
    
        [x] Protect the list view for the Receipt Model so that only a person that has logged in can access it
        [x] Change the queryset of the view to filter the reciept objects where purchaser equals the logged in user 

# Feature 9 Logout
    [x] in accounts/urls.py
        [x] Import Logoutview from the same modules that you imported the login view from
        [x] register that view in url patterns list with the path "logout/" and the name logout
        [x] in the expenses settings.py create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page

# Feature 10 Signup
- This feature allows people to sign up for the project tracker.
- You need to create a function view to handle showing the sign-up form, and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.

        [x] import USERCREATIONFORM from built in auth forms
        [x] use special create user method to create a new user from their username and password
        [x] use login function that logs an account in
        [x] after you have created the user redirect the broswer to the path registered with the name "home"
        [x] Create an html template named signup.html in registration directory
        [x] put post form in signup.html and other template requirements

## Template Specs
    
    [x] fun five
    [x] main tag
        [x] div
            [x] h1 - SIGNUP
            [x] form -method post 
                [x] input  -text -username
                [x] input  -password -password
                [x] input -password -password2
                [x] button content-"signup"

# Feature 11 Create Receipt View
- This feature allows a person to go from the receipt list page to a page that allows them to create a new receipt.
    
        [x] Create a view for the Receipt Model - fields [ vendor, total, tax, date, category, account] handle submission to create a new receipt
        [x] Person must be logged in to see view
        [x] Register the view for path "create/" in reciepts urls.py name="create_reciept"
        [x] Create Html Template 

- If you are using class views, you can set the user by writing your own form_valid method. The general form of a method that assigns the current user to a User property looks like this.
      
      def form_valid(self, form):
        item = form.save(commit=False)
        item.user_property = self.request.user
        item.save()
        return redirect("some_view")

## Create Template 

    [x] Fun Five
    [x] main
        [x] div
            [x] h1 Create Recipe
            [x] form tag - method post
            [x] input tag type text name vendor
            [x] input tag type text name total
            [x] input tag type text name tax 
            [x] input tag type text name date
            [x] select tag name category
            [x] select tag name account
            [x] button Create

# Feature 12 List Views
-Create the rest of the list views.
-To access any list view, a person must be logged in.

    - For the list views, display the data in tables. Make sure that you filter the data for only the current user. You can do this by creating your own get_queryset method on the views and using the general form:

    def get_queryset(self):
    return Model.objects.filter(user_property=self.request.user)
    
    You'll need to refer to each model for what the name of the user_property is on that model, the one that is a foreign key to the User object.

## Expense Category List View
* path receipts/categories

        [x] For the ExpenseCategory list, have it show a table that has the columns of the name and the number of receipts associated to that category.  

## Accounts List View
* path receipts/accounts
  
        [x] For the Account list, have it show a table that has the columns of the name, number, and the number of receipts associated to that account. 

# Feature 13 Create View Category
  
    [x] Create CreatView for ExpenseCategory
        [x] Name property 
        [x] must be logged on to view
        [x] register view path "categories/create/" reciepts/urls.py name "create_category"
        [x] Expense Category Template

* If you are using class views, you can set the user by writing your own form_valid method. The general form of a method that assigns the current user to a User property looks like this.

        def form_valid(self, form):
            item = form.save(commit=False)
            item.user_property = self.request.user
            item.save()
            return redirect("some_view")

## Template Specs
    [x] Base
        [x] Div
            [x] H1
            [x] form - method post
                [x] input text name "name"
                [x] button create
 
 # Feature 14 Create Account View
    
    [ ] CreateView for Account Model
        [ ] field Name and number
    [ ] must be logged in to see
    [ ] register "accounts/create/" in receipts urls.py name="create_account"
    [ ] Create Template

* If you are using class views, you can set the user by writing your own form_valid method. The general form of a method that assigns the current user to a User property looks like this.

         def form_valid(self, form):
            item = form.save(commit=False)
            item.user_property = self.request.user
            item.save()
            return redirect("some_view")

## Create Form
    
    [ ] Base
        [ ] div
            [ ] h1
            [ ] form
                [ ] method 'post'
                [ ] input type text name name
                [ ] input type text name number
                [ ] button create